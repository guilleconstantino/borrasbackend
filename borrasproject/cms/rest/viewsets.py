'''
@author: gconstantino
'''

#####
from cms import models
from cms.rest import serializers
from rest_framework import viewsets



class PromotionViewSet(viewsets.ModelViewSet):
    """
    An API view that allows to display ALL the promotions on the cms
    """
    queryset = models.Promotion.objects.all()
    serializer_class = serializers.PromotionSerializer
    