from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _



class Promotion(models.Model):
    """
    A promotion is an offer for a product along a period of time.
    """
    description = models.CharField(verbose_name=_('Description'), max_length=255)
    image = models.ImageField(verbose_name=_("Image"), null=True, blank=True)
    expiryDate = models.DateTimeField(verbose_name=_('Expiry Date'))
    paymentMethods = models.TextField(verbose_name=_('Payment Methods'), null=True, blank=True)
    