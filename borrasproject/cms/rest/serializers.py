'''

@author: gconstantino
'''

from rest_framework import serializers
from cms import models


class PromotionSerializer(serializers.HyperlinkedModelSerializer):
    """
    It is the representation of the Promotion model that will show through the API
    """
    class Meta:
        model = models.Promotion
        fields = ('description', 'image', 'expiryDate', 'paymentMethods')
        