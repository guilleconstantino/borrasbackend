PROJECT_DIR="borrasproject"
SETTINGS_SUBDIR="borrasproject"


. ./envdjango/bin/activate

# Set db
python $PROJECT_DIR/manage.py migrate
python $PROJECT_DIR/manage.py createsuperuser

# cp $PROJECT_DIR/$SETTINGS_SUBDIR/test_settings.py $PROJECT_DIR/$SETTINGS_SUBDIR/local_settings.py

echo "Do you wish to set test data into the DB? Select a number"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) python $PROJECT_DIR/manage.py loaddata $PROJECT_DIR/fixtures/test.json; break;;
        No ) exit;;
    esac
done

