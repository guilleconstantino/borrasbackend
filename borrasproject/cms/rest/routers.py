'''
@author: gconstantino
'''

#####
from rest_framework import routers
from cms.rest import viewsets

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'promotions', viewsets.PromotionViewSet)
